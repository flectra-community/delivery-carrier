# Flectra Community / delivery-carrier

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[delivery_package_number](delivery_package_number/) | 2.0.1.1.0| Set or compute number of packages for a picking
[delivery_carrier_city](delivery_carrier_city/) | 2.0.1.0.0| Integrates delivery with base_address_city
[delivery_roulier](delivery_roulier/) | 2.0.1.0.1| Integration of multiple carriers
[delivery_cttexpress](delivery_cttexpress/) | 2.0.1.0.0| Delivery Carrier implementation for CTT Express API
[delivery_roulier_option](delivery_roulier_option/) | 2.0.1.0.0| Add options to roulier modules
[carrier_account_environment](carrier_account_environment/) | 2.0.1.0.1| Configure carriers with server_environment_files
[delivery_postlogistics_dangerous_goods](delivery_postlogistics_dangerous_goods/) | 2.0.1.0.0| Declare dangerous goods when generating postlogistics labels
[delivery_price_rule_untaxed](delivery_price_rule_untaxed/) | 2.0.1.0.2| Add untaxed amount to variables for price delivery price rule
[delivery_package_fee](delivery_package_fee/) | 2.0.1.0.2| Add fees on delivered packages on shipping methods
[delivery_carrier_customer_info](delivery_carrier_customer_info/) | 2.0.1.0.0| Send delivery notice to the shipper from any operation.
[delivery_carrier_agency](delivery_carrier_agency/) | 2.0.1.0.1| Add a model for Carrier Agencies
[stock_picking_carrier_from_rule](stock_picking_carrier_from_rule/) | 2.0.1.0.0|     Set the carrier on picking if the stock rule used has a partner    address set with a delivery method.    
[delivery_carrier_package_measure_required](delivery_carrier_package_measure_required/) | 2.0.1.0.1|     Allow the configuration of which package measurements are required    on a delivery carrier basis.    
[delivery_schenker](delivery_schenker/) | 2.0.1.1.1| Delivery Carrier implementation for DB Schenker API
[delivery_carrier_location](delivery_carrier_location/) | 2.0.1.0.0| Integrates delivery with base_location
[delivery_postlogistics_server_env](delivery_postlogistics_server_env/) | 2.0.1.0.0| Server Environment layer for Delivery Postlogistics
[delivery_postlogistics](delivery_postlogistics/) | 2.0.1.0.2| Print PostLogistics shipping labels using the Barcode web service
[base_delivery_carrier_files](base_delivery_carrier_files/) | 2.0.1.0.0| Base module for creation of delivery carrier files
[delivery_roulier_laposte_fr](delivery_roulier_laposte_fr/) | 2.0.1.0.1| Generate Label for La Poste/Colissimo
[delivery_carrier_category](delivery_carrier_category/) | 2.0.1.0.0|         Adds a category to delivery carriers in order to help users        classifying them
[base_delivery_carrier_label](base_delivery_carrier_label/) | 2.0.1.3.0| Base module for carrier labels
[delivery_free_fee_removal](delivery_free_fee_removal/) | 2.0.1.0.0| Hide free fee lines on sales orders
[delivery_schenker_picking_volume](delivery_schenker_picking_volume/) | 2.0.1.1.1| Glue module between delivery_schenker and stock_picking_volumeWith this module the transmitted volume is changed,it uses the computed volume from stock_picking_volume
[delivery_carrier_multi_zip](delivery_carrier_multi_zip/) | 2.0.1.0.0| Multiple ZIP intervals for the same delivery method
[delivery_tnt_oca](delivery_tnt_oca/) | 2.0.1.2.3| Integrate TNT webservice
[delivery_carrier_partner](delivery_carrier_partner/) | 2.0.1.0.0| Add a partner in the delivery carrier
[delivery_send_to_shipper_at_operation](delivery_send_to_shipper_at_operation/) | 2.0.1.0.1| Send delivery notice to the shipper from any operation.
[stock_picking_delivery_link](stock_picking_delivery_link/) | 2.0.1.0.0| Adds link to the delivery on all intermediate operations.
[delivery_multi_destination](delivery_multi_destination/) | 2.0.1.1.2| Multiple destinations for the same delivery method
[delivery_auto_refresh](delivery_auto_refresh/) | 2.0.1.1.0| Auto-refresh delivery price in sales orders
[delivery_carrier_pricelist](delivery_carrier_pricelist/) | 2.0.1.0.2| Compute method method fees based on the product's pricelist.
[server_environment_delivery](server_environment_delivery/) | 2.0.1.0.0| Configure prod environment for delivery carriers
[partner_delivery_zone](partner_delivery_zone/) | 2.0.1.1.0| Set on partners a zone for delivery goods
[delivery_correos_express](delivery_correos_express/) | 2.0.1.0.1| Delivery Carrier implementation for Correos Express using their API
[delivery_state](delivery_state/) | 2.0.1.1.0| Provides fields to be able to contemplate the tracking statesand also adds a global fields
[partner_default_delivery_carrier](partner_default_delivery_carrier/) | 2.0.1.0.0| Allows defining default delivery methods for partners
[delivery_carrier_label_batch](delivery_carrier_label_batch/) | 2.0.1.1.0| Carrier labels - Stock Batch Picking (link)
[delivery_carrier_info](delivery_carrier_info/) | 2.0.1.0.1| Add code and description on carrier
[delivery_carrier_default_tracking_url](delivery_carrier_default_tracking_url/) | 2.0.1.0.0|         Adds the default tracking url on delivery carrier
[delivery_price_method](delivery_price_method/) | 2.0.1.0.0| Provides fields to be able to contemplate the tracking statesand also adds a global fields


