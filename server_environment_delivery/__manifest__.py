# Copyright 2020 Camptocamp SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl)
{
    "name": "Server Environment Delivery",
    "summary": "Configure prod environment for delivery carriers",
    "version": "2.0.1.0.0",
    "development_status": "Alpha",
    "category": "Operations/Inventory/Delivery",
    "website": "https://gitlab.com/flectra-community/delivery-carrier",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": ["delivery", "server_environment"],
}
