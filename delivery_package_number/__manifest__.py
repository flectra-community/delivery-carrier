# Copyright 2020 Tecnativa - David Vidal
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Stock Picking Package Number",
    "summary": "Set or compute number of packages for a picking",
    "version": "2.0.1.1.0",
    "category": "Delivery",
    "website": "https://gitlab.com/flectra-community/delivery-carrier",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "installable": True,
    "application": False,
    "depends": ["delivery"],
    "data": [
        "views/stock_picking_views.xml",
        "wizard/stock_immediate_transfer_views.xml",
    ],
}
