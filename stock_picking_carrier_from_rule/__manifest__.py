{
    "name": "Stock Picking Carrier From Rule",
    "summary": """
    Set the carrier on picking if the stock rule used has a partner
    address set with a delivery method.
    """,
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/delivery-carrier",
    "depends": ["delivery"],
    "installable": True,
}
