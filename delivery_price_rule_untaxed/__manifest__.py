# Copyright 2018 Simone Rubino - Agile Business Group
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Delivery price rule untaxed",
    "summary": "Add untaxed amount to variables for price delivery price rule",
    "version": "2.0.1.0.2",
    "category": "Stock",
    "website": "https://gitlab.com/flectra-community/delivery-carrier",
    "author": "Agile Business Group, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": ["delivery"],
    "maintainers": ["AshishHirapara"],
    "demo": ["data/delivery_price_rule_untaxed_demo.xml"],
}
