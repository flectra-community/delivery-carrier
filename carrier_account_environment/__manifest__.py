# Copyright 2019 David BEAL @ Akretion
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Carrier accounts configuration with server_environment",
    "version": "2.0.1.0.1",
    "category": "Tools",
    "summary": "Configure carriers with server_environment_files",
    "maintainers": ["florian-dacosta"],
    "author": "Akretion, Camptocamp, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/delivery-carrier",
    "depends": [
        "server_environment",
        "base_delivery_carrier_label",
    ],
}
