# Copyright 2021 Camptocamp
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Partner Default Delivery Method",
    "summary": "Allows defining default delivery methods for partners",
    "version": "2.0.1.0.0",
    "category": "Delivery",
    "website": "https://gitlab.com/flectra-community/delivery-carrier",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "maintainers": ["SilvioC2C"],
    "installable": True,
    "license": "AGPL-3",
    "depends": ["delivery", "sale"],
    "data": ["views/res_config_settings.xml"],
}
