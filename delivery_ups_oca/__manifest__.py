# Copyright 2020 Hunki Enterprises BV
# Copyright 2021 Tecnativa - Víctor Martínez
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Delivery UPS OCA",
    "summary": "Integrate UPS webservice",
    "version": "1.0.1.0.1",
    "development_status": "Production/Stable",
    "category": "Delivery",
    "website": "https://gitlab.com/flectra-community/flectra",
    "author": "Hunki Enterprises BV, Tecnativa, Odoo Community Association (OCA), Jamotion GmbH",
    "license": "AGPL-3",
    "excludes": ["delivery_ups"],
    "depends": [
        "delivery",
        "delivery_package_number",
        "delivery_price_method",
        "delivery_state",
    ],
    "data": ["data/product_packaging_data.xml", "views/delivery_carrier_view.xml"],
    "demo": [],
}
